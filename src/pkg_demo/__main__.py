""" __main__ to load package as module """
import logging
import sys


def main():
    """
    Main entry point.
    """
    logging.debug("Arguments received: %s", sys.argv[1:])
    print("hello")


if __name__ == "__main__":
    main()

""" Package pkg_demo top level """
import logging
import pkg_resources

__version__ = pkg_resources.get_distribution("pkg_demo").version

logging.getLogger(__name__).addHandler(logging.NullHandler())

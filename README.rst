========
pkg-demo
========
A minimal example project to demonstrate packaging.

********
Features
********
* Minimal setup.py with configuration in setup.cfg
* Uses Pipfile over requirements.txt
* Single source versioning using distribution information
* Demonstrates project used as module and console entry point
* Properly open source example (license!)
* Uses src-layout
* Python 2 and 3 compatible
* Verified with:

  * check-manifest
  * pylint
  * pyroma
  * twine check

*****
Usage
*****
An example of how to build and publish a Python distribution.
The name of the package will have to be changed in order to work.
And you will need to have an account on https://test.pypi.org/.

.. code-block:: bash

   git clone git@gitlab.com:digitalr00ts-demos/pkg-demo.git
   cd pkg-demo
   pipenv install --dev
   pipenv build
   pipenv publish

******
To-dos
******
* tox.ini
* docs
* tests
* sign distribution

*******
License
*******
MIT License

